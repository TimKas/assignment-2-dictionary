%include "lib.inc"
%include "words.inc"
%include "dict.inc"
 
%define buffSize 256
%define dSize 8
%define EXIT_CODE_ERROR 1

section .rodata
READING_ERROR: db "error occured while reading from stdin", 0xA, 0
KEY_NOT_FOUND: db "entered key does not exist in the dictionary", 0xA, 0
buf: resb buffSize
 
section .text
 
global _start



_start:
 
    ; trying to write entered key to the buffer 
    mov rdi, buf
    mov rsi, buffSize
    call read_word
    test rax, rax
    jz .reading_error
    
    ; trying to find entered key
    mov rdi, buf
    mov rsi, entry_with_long_key
    call find_word
    test rax, rax
    jz .key_not_found
    
    ; printint out the content the found key is pointing to
    mov rdi, rax
    add rdi, dSize
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    xor rdi, rdi
    jmp exit
    
    ; exception occured while reading from stdin
    .reading_error:
        mov rdi, READING_ERROR
        jmp .end_with_error
 
    ; during the traversal the key was not found 
    .key_not_found:
        mov rdi, KEY_NOT_FOUND
 
    .end_with_error:
        call print_err_str
        mov rdi, EXIT_CODE_ERROR
        jmp exit
