import subprocess

inpt = ["", "first", "second", "nonesense", "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"]
expected_out = ["", "first word", "second word",  "", ""]
expected_err = ["entered key does not exist in the dictionary", "", "", "entered key does not exist in the dictionary", "error occured while reading from stdin"]

for i in range(len(inpt)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=inpt[i].encode())
    out_result = stdout.decode().strip()
    err_result = stderr.decode().strip()
    if out_result == expected_out[i] and err_result == expected_err[i]:
        print("Test ["+str(i+1)+"] +")
    else:
        print("Test "+str(i+1)+' failed {strout: "'+out_result+'", strerr: "'+err_result+'"}, expected: {strout: "'+expected_out[i]+'", strerr: "'+expected_err[i]+'"}')

