global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global exit
global string_length
global print_string
global print_err_str
global print_char
global print_newline



section .text


exit:
    mov rax, 60
    syscall


string_length:
    xor rax, rax
    .counter:
        inc rax
        cmp byte[rdi+rax-1], 0
        jne .counter
    dec rax
    ret


print_err_str:
	push rdi
	call string_length
	pop rsi ; pointer
	mov rdx, rax ; length
	mov rax, 1 ; write sys
	mov rdi, 2 ; stderr
	syscall
	ret


print_string:
    push rdi
    call string_length
    pop rsi ; pointer
    mov  rdx, rax ; length
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

print_newline:
    mov rdi, 0xA


    print_char:
        xor    rax, rax
        mov    rax, 1
        mov     rdx, 1
        push    rdi
        mov    rdi, 1
        mov    rsi, rsp
        syscall
        pop    rdi
        ret


    print_int:
        cmp    rdi, 0
        jge    .end
        push    rdi
        mov    rdi, '-'
        call    print_char
        pop    rdi
        neg    rdi
        .end:
        call    print_uint
        ret



    print_uint:
        xor    rax, rax
        push    rax
        push    rax
        push    rax
       
        mov    rax, rdi
        mov    rdi, rsp
        add    rdi, 3*8-1
        mov    rcx, 10
       
        .while:
            dec    rdi
            xor    rdx, rdx
            div    rcx
            add    dl, 0x30
            mov    [rdi], dl
            .compare:
            cmp    rax, 0x00
            jne    .while

        .quit:
        call    print_string
        add    rsp, 3*8
        ret

       

    string_equals:
        xor    rax, rax
        .looping:
            mov    r12b, byte[rdi+rax]
            cmp    r12b, byte[rsi+rax]

            jne    .ending_not_equal
        ; one of them equals 0 -> they both equal 0 -> halt
            cmp    byte[rdi+rax], 0
            je    .ending_equal
        ; continue
            inc    rax
            jmp    .looping

        .ending_equal:
            mov     rax, 1
            ret
        .ending_not_equal:
            xor    rax, rax
            ret
   

    read_char:
        xor     rax, rax
        mov    rdi, 0
        push    0
        mov    rsi, rsp
        mov    rdx, 1
        syscall
        cmp    rax, 0
        jle    .error ; if rax <= 0 then error
        pop    rax
        jmp .end

        .error:
            pop    rax
            xor    rax, rax
        .end:
            ret


    read_word:
        push    r14
        push    r15
        xor    r14, r14
        mov    r15, rsi
       
        dec    r15

        cmp    rsi, 1
        jle    .exception

        .while:
            push    rdi
            call    read_char
            pop    rdi           
            cmp    al, ' '
            je    .while
            cmp    al, 10
            je    .while
            cmp    al, 13
            je    .while
            cmp    al, 9
            je    .while
            test    al, al
            jz    .success

        .chars:
            mov    byte[ rdi + r14 ], al
            inc     r14
            push    rdi
            call    read_char
            pop    rdi   
            cmp    al, ' '
            je    .success
            cmp    al, 10
            je    .success
            cmp    al, 13
            je    .success
            cmp    al, 9
            je    .success
            test    al, al
            jz    .success
            cmp    r14, r15
            jge    .exception

            jmp    .chars

        .exception:
            xor    rax, rax
            jmp    .end
           
        .success:
            mov    byte[ rdi + r14 ], 0
            mov    rax, rdi
            mov    rdx, r14
           
        .end:
            pop    r15
            pop     r14
            ret
 


    parse_uint:
        ; number of read numbers rdx
        xor    rax, rax
        xor    rcx, rcx
        mov    r8, 10
        .next_number:
            movzx    r9, byte[rdi+rcx]
            cmp    r9b, '0'
            jb     .end
            cmp    r9b, '9'
            ja    .end
            xor    rdx, rdx
            mul    r8
            and    r9b, 0x0f
            add    rax, r9   
            inc    rcx
            jmp    .next_number
        .end:
        mov    rdx, rcx
        ret



    parse_int:
        cmp    byte[rdi], '-'
        je    .neg
        cmp    byte[rdi], '+'
        je    .pos
        call    parse_uint
        jmp    .end

        .neg:
            inc    rdi
            call    parse_uint
            neg    rax
            inc    rdx
            jmp    .end
        .pos:
            inc    rdi
            call    parse_uint
            jmp    .end
        .end:
            ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
    string_copy:
        ; rdi string starts at
        ; rsi buffer starts at
        ; rdx max buffer length
        ; returns rax length of the string and 0 if error

        push    r13
        xor    r13, r13
        cmp    rdx, 0
        je    .halt_with_exception
       
        .check_and_copy:
            mov    rax, [rdi+r13]
            mov    [rsi+r13], rax
            cmp    byte[rdi+r13], 0
            je    .halt_successful
            inc    r13
            cmp    r13, rdx
            je    .halt_with_exception
            jmp    .check_and_copy

        .halt_successful:
            mov    rax, r13
            jmp    .full_stop

        .halt_with_exception:
            mov    rax, 0
            jmp    .full_stop

        .full_stop:
            pop    r13   
            ret
            

	

