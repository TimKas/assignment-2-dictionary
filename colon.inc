%define last_id 0

%macro colon 2
	%ifid %2
		%ifstr %1
			%2:
				dq last_id
				db %1, 0
			%define last_id %2
		%else
			%error "the first argument is not a string"
		%endif
	%else
		%error "the second argument is not a label"
	%endif
%endmacro
