%include "lib.inc"
global find_word
 
section .text
 
%define LABEL_LENGTH 8
 
; Accepts two arguments:
; rdi pointer to null-terminated string.
; rsi pointer to the start of the dict.
; if the right entry is found, returns in acc the address of the pointer to the entry, otherwise rax: 0.
 
find_word:
        ; save registers that will be utillized
        push rsi
        push rdi
 
        ; rsi will always pointer to the label of the current entry
        ; so that we could get the pointer of the next entry it is pointing to
        .next_key:
                add rsi, LABEL_LENGTH
                call string_equals
		mov r10, rax
                sub rsi, LABEL_LENGTH
		test r10, r10
                jnz .key_found
 
        cmp qword[rsi], 0
        je .no_such_key
        mov rsi, qword[rsi]
        jmp .next_key
 
        .no_such_key:
                xor rax, rax
                jmp .exit
        .key_found:
                mov rax, rsi
        .exit:
                pop rdi
                pop rsi
                ret
