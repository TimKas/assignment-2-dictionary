ASM=nasm
ASMFLAGS=-f elf64
LD=ld
RM=rm
RM_FLAGS = -rf


main: main.o dict.o lib.o
	$(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	$(RM) $(RM_FLAGS) *.o main

run: all
	./main && echo "OK!" || echo "FAIL!"

test: all
	python test.py

.PHONY: clean all run test
